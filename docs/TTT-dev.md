# TTT - Dev documentation v.03



## Getting started

### Introduction

TTT is a Blockchain platform for companies wishing to leverage the potential of Distributed Applications in their day to day Operations. It's developed as a platform for assets.

Every asset has a representation in tokens and users could launch their own Branded Tokens to turn businesses into a dynamic ecosystem.

TTT aims to be the blockchain technology partner of choice for businesses of all sizes and levels of technical sophistication, enabling any company to create, launch, and manage their own branded digital token economy powered by Mbaex protocols and TTT blockchain management software

Main differences with the public Ethereum:

- It's a Proof of Authority network. We will have a set of permissioned validators.
- The native token (aka TTT) won’t act as Gas. We will have a bank of Gas giving away gas to send transactions, deploy Smart Contract or to Call functions. This way we can have improved control over the usage of the network.
- The native token will be the right to use and decide on the future of the platform. You need TTTs to buy credits and vote for changes in the network. TTTs are voting rights. You will also need TTTs for basic actions in the network as running a Node, using the Name Service, adding a Token to the WhiteList...
- The main coin on the system will be USDP (United States Dollar Pegged). Banks will certify these coins and will be audited by external Auditors.
- All actors in the ecosystem need a valid ID: validators, regular nodes, tokens... We will have a simple Self Sovereign ID (SSID) system based on ERC725v2.
- Credits (gas) to work on the system (deploy, call, tx) will be given on a timed basis (daily, weekly…) to all the holders of TTT. These credits can be either used or sold at the Credits market at a fixed price. This price can be changed by voting amongst TTTs owners.

## Network and configuration

### Node requirements

- Internet access.
- Previous use of command line interface and Unix commands.
- Ubuntu 18.04 LTS version or higher
- 4GB of RAM or more, 40GB SSD

To proceed the reader must be logged into a server using an user account with elevated privileges.

> Please contact [Caelum Labs team](mailto:alex@caelumlabs.com) for support on installing in other supported OS.

### Nodes and Network

The peer-to-peer network in blockchain is structured according to the consensus mechanism that they are utilizing. For cryptos like Bitcoin and Ethereum which uses a normal proof-of-work consensus mechanism (Ethereum will eventually move on to Proof of Stake), all the nodes have the same privilege. The idea is to create an egalitarian network. The nodes are not given any special privileges, however, their functions and degree of participation may differ. There is no centralized server/entity, nor is there any hierarchy. It is a flat topology.
These decentralized cryptocurrencies are structured like that is because of a simple reason, to stay true to their philosophy. The idea is to have a currency system, where everyone is treated as an equal and there is no governing body, which can determine the value of the currency based on a whim. This is true for both bitcoin and Ethereum.

#### Validator Node

A validator node is a program that fully validates transactions and blocks. Almost all validator nodes also help the network by accepting transactions and blocks from other validator nodes, validating those transactions and blocks, and then relaying them to further validator nodes.
Most validator nodes also serve regular nodes by allowing them to transmit their transactions to the network and by notifying them when a transaction occurs.

#### Regular Node

A regular node is a piece of software that connects to validator nodes to interact with the blockchain. Unlike their validator node counterparts, regular nodes don’t need to run 24/7 or read and write a lot of information on the blockchain. In fact, regular nodes do not interact directly with the blockchain; they instead use validator nodes as intermediaries. Regular nodes rely on validator nodes for many operations, from requesting the latest headers to asking for the balance of an account.

#### Token node

A Token node is a regular node that also manage one token inside the network. These are crucial actors for the ecosystem as they can deploy ERC20 tokens and ERC725 smart contracts (self-sovereign identity)
![](img/SlZZnfV.jpg)

### Setup a regular node

For setting up a regular node go to the `network` folder just cloned and run:

```
    ./regular.sh
```

Then run then node:

```
    parity --config ~/.local/share/io.parity.ethereum/regular.toml
```

At the moment you do not have any account imported, so your node it is pretty useless.

Import an account if you want to interact with the blockchain.

```
    cd ~/.local/share/io.parity.ethereum
    parity account new --chain spec.json
```

### Setup an validator node

For setting up a validator node go to the `network` folder just cloned and run:

```
    ./authority.sh
```

Also, you need an Ethereum account which is a key-pair value that identifies you as a person and/or company.

In order to get a new key-pair we recommend to use:

```
    cd ~/.local/share/io.parity.ethereum
    parity account new --chain spec.json
```

The key-pair you've created is the Ethereum account that you use to issue new blocks when the network government accepts you as an authority.

But you cannot issue new blocks because you are not in the permissioned whitelist. Contact the network administrator in order to be registered as authorised validator. For the moment, send a mail to [*@Network Admin*](mailto:mbaex@caelumlabs.com) with the account public key you want to use in order to validate transactions. In the future the network government will be controlled by a smart contract based democracy system.

Your account needs to have a password file that contains the password of the UTC file. This password is the same you put in the creation of the key-pair. We recommend to put this file in a secure directory, for example in:

```
    /root/.parity/pass
```

At this moment you are able to run your authority node typing:

```
    bash
    cd ~/.local/share/io.parity.ethereum && parity --config authority.toml
```

### Setup a token node

For setting up a regular node go to the `network` folder just cloned and run:

```
    ./regular.sh
```

Then run then node:

```
    parity --config ~/.local/share/io.parity.ethereum/regular.toml
```

At the moment you do not have any account imported, so your node it is pretty useless.

Import an account if you want to interact with the blockchain.

```
    cd ~/.local/share/io.parity.ethereum
    parity account new --chain spec.json
```

## Platform

### Introduction

A blockchain is a data structure in a [*peer-to-peer network*](https://en.wikipedia.org/wiki/Peer-to-peer) of interconnected computers. The two most important features of this data storage system are that it is byzantine fault tolerant and distributed.

Any computer with access to this network can send transactions that are then stored into *blocks* by specific computers, known as authority nodes.

In [*proof-of-work*](http://nakamotoinstitute.org/bitcoin/) based blockchain networks a computer with the ability to create a block of transactions is called *miner*. Miners compete between each other by using computational power to be the first to find the next valid block for the blockchain.

Since competition between nodes is not desired in private chains,  the network reaches consensus via a *proof-of-authority algorithm* named [Aura](https://github.com/paritytech/parity/wiki/Aura).

The authorities take turns signing a new block. These blocks are immutable after more then 50% of the authorities signed them twice.

Regular or non-validator nodes can send transactions to the network and/or can create new contracts, but in any case can be able to sign a new block or vote in the network government. This type of admin actions can only be performed by authority nodes.

### Parity client

Parity supports a Proof-of-Authority consensus engine to be used with EVM based chains. Proof-of-Authority is a replacement for Proof-of-Work, which can be used for private chain configurations.

For consortium setting there are no disadvantages of PoA network as compared to PoW.

It is more secure (since an attacker with unwanted connection or hacked authority can not overwhelm a network potentially reverting all transactions), less computationally intensive (mining with difficulty which provides security requires lots of computation), more performant (Aura consensus provides lower transaction acceptance latency) and more predictable (blocks are issued at steady time intervals).

PoA deployments are used by the enterprise and by the public (e.g. popular Kovan test network).

### Governance

In these alpha version of the platform the validator selection has been done via a config file that will be shared to:

- Control the governance of the network
- Manage who is a validator node and who don't

In the future the governance of TTT will be called it "Gas economy" as it will work as a fuel for the network.

There will be packages of tokens depending on the service willing to perform.

To run a node the user will need a token package as the following example:

- Name Service entry: 5,000 TTT
- Regular node: 10,000 TTT
- Validator node: 500,000 TTT

*These numbers are an approximative depending on the token supply.*

Based on the  Aura consensus protocol used on the network, if one node has to be removed from the network and a new one to be added, the other validators nodes have to vote for it. The TTT token will be used for the voters.

### Network infrastructure

The network configuration at the moment is composed by 5 nodes. This nodes have different features. The most important for us at this point of the projects is JSON-RPC, summarizing is a data exchange protocol that allows a client (for example: a dapp) to communicate to a server (your local blockchain node) by issuing commands and listening to responses.

In this case just the regular node have the RPC open so they can interact with third parties, the other nodes validator and token have the rpc closed for security reasons, these nodes don't need to interact with third parties.

This is the list of nodes for the platform nowadays:

- [Validator node](http://validator1.tttplatform.com)
- [Regular node](http://regular1.tttplatform.com)
- [Regular node](http://regular2.tttplatform.com)
- [Regular node](http://regular3.tttplatform.com)
- [Token node (TTT)](http://token1.tttplatform.com)

Each node is protected with a user an password, and could be managed via a front-end.

| Node        | User            | Password |
| ----------- | --------------- | -------- |
| Validator 1 | admin@admin.com | admin    |
| Regular 1   | test@test.com   | 123456   |
| Regular 2   | test@test.com   | 123456   |
| Regular 3   | test@test.com   | 123456   |
| Token 1     | admin@admin.com | admin    |



### Network Smart Contracts architecture

![](img/archt.png)

### Gas bank

#### Overview

**TTT Gas Bank** (TTTGB) is a smart contract that enables users to request for gas. Users must be in the smart contract whitelist and comply with two requirements:

- The amount blocks sealed since last gas request needs to be higher than block threshold
- The amount of gas that an account has must be equal or less than the gas threshold

#### Uses

##### Anti-DDoS

Since gas is spent at every iteration of a loop, which ensures that there can be no infinite loop since all the gas in any designated account(wallet) is bound to get empty sooner or later. Whenever the wallet is indeed empty and the loop is terminated. This ensures and protects the Blockchain against DDoS attacks.

##### Usability

Users haven't got to manage gas as this is non usable, it will be the token (dapp) who will manage it itself and distribute it to the users as needed.  The user will never manage gas or fees.

#### Work flow

1. An administrator deploys the GasBank using as params:
   - a list of accounts to add into whitelist
   - a wei threshold amount
   - a block threshold
   - the amount of gas to send on every request.
2. Gas bank needs TTT, anyone can block TTT into the gas bank smart contract to get into the whitelist.
3. The owner (administrator) of the contract can add or remove whitelisted accounts
4. Any user in the whitelist can request gas calling to the function request_gas
5. If the user complies with the thresholds the amount defined is sent to the user

#### Limitations, or not.

Block threshold and the amount to send on gas request is the same for every whitelisted user

#### Usage

 Deploy on your own network or pass tests

```
npm i && npm test
```

Gas Bank address: "0x0591F2AbE35bd491b6E721Ac294e10453aC73Ce9"

### Ethereum Name service

ENS is the Ethereum Name Service, a distributed, open, and extensible naming system based on the Ethereum blockchain.

ENS’s job is to map human-readable names like ‘alice.eth’ to machine-readable identifiers such as Ethereum addresses, content hashes, and metadata. ENS also supports ‘reverse resolution’, making it possible to associate metadata such as canonical names or interface descriptions with Ethereum addresses.

ENS has similar goals to DNS. Like DNS, ENS operates on a system of dot-separated hierarchical names called domains, with the owner of a domain having full control over subdomains.

Top-level domains, like ‘.eth’ and ‘.test’ are owned by smart contracts called registrars, which specify rules governing the allocation of their subdomains. Anyone may, by following the rules imposed by these registrar contracts, obtain ownership of a domain for their own use.

Because of the hierarchical nature of ENS, anyone who owns a domain at any level may configure subdomains - for themselves or others - as desired. For instance, if Alice owns 'alice.eth', she can create 'pay.alice.eth' and configure it as she wishes.



#### Architecture

ENS has two principal components: the [registry](https://docs.ens.domains/contract-api-reference/ens), and [resolvers](https://docs.ens.domains/contract-api-reference/publicresolver).

![](img/ens1.png)

The ENS registry consists of a single smart contract that maintains a list of all domains and subdomains, and stores three critical pieces of information about each:

> - The owner of the domain
> - The resolver for the domain
> - The caching time-to-live for all records under the domain

The owner of a domain may be either an external account (a user) or a smart contract. A registrar is simply a smart contract that owns a domain, and issues subdomains of that domain to users that follow some set of rules defined in the contract.

Owners of domains in the ENS registry may:

> - Set the resolver and TTL for the domain
> - Transfer ownership of the domain to another address
> - Change the ownership of subdomains

The ENS registry is deliberately straightforward and exists only to map from a name to the resolver responsible for it.

Resolvers are responsible for the actual process of translating names into addresses. Any contract that implements the relevant standards may act as a resolver in ENS. General-purpose resolver implementations are offered for users whose requirements are straightforward, such as serving an infrequently changed address for a name.

Each record type - Ethereum address, Swarm content hash, and so forth - defines a method or methods that a resolver must implement in order to provide records of that kind. New record types may be defined at any time via the EIP standardization process, with no need to make changes to the ENS registry or to existing resolvers in order to support them.

Resolving a name in ENS is a two-step process: First, ask the registry what resolver is responsible for the name, and second, ask that resolver for the answer to your query.

![](img/ens2.png)

In the above example, we're trying to find the Ethereum address pointed to by 'foo.eth'. First, we ask the registry which resolver is responsible for 'foo.eth'. Then, we query that resolver for the address of 'foo.eth'.



## Identities

### Introduction to SSI

SSI could be defined as a new concept of individuals or organizations having sole ownership of their digital and analog identities, and control over how their personal data is shared and used.
These principles attempt to ensure the user control that's at the heart of self-sovereign identity. However, they also recognize that identity can be a double-edged sword — usable for both beneficial and maleficent purposes. Thus, an identity system must balance transparency, fairness, and support of the commons with protection for the individual.

- Existence: Users must have an independent existence.
- Control: Users must control their identities.
- Access: Users must have access to their own data.
- Transparency: Systems and algorithms must be transparent.
- Persistence: Identities must be long-lived.
- Portability: Information and services about identity must be transportable.
- Interoperability: Identities should be as widely usable as possible.
- Consent: Users must agree to the use of their identity.
- Minimalization: Disclosure of claims must be minimized.
- Protection: The rights of users must be protected.

### Uses

Each user will have a unique smart contract (following the ERC725 standard) so all tokens will be assigned to the address of this smart contract.

This strategy will allow to:

- Create also token identity : owner, certificates...
- Shared User certification: KYC & AML
- recover funds: by recovering control over your Identity Smart Contract.

### Identity smart contract: ERC725

 **Definitions**

- `keys`: Keys are public keys from either external accounts, or contracts' addresses.

 **Specification**

 **Key Management**

Keys are cryptographic public keys, or contract addresses associated with this identity.

The structure should be as follows:

- ```
  key
  ```

  : A public key owned by this identity

  - `purpose`: `uint256[]` Array of the key types, like 1 = MANAGEMENT, 2 = EXECUTION
  - `keyType`: The type of key used, which would be a `uint256` for different key types. e.g. 1 = ECDSA, 2 = RSA, etc.
  - `key`: `bytes32` The public key. // for non-hex and long keys, its the Keccak256 hash of the key

```
struct Key {
    uint256[] purposes;
    uint256 keyType;
    bytes32 key;
}
```

 **getKey**

Returns the full key data, if present in the identity.

```
function getKey(bytes32 _key) constant returns(uint256[] purposes, uint256 keyType, bytes32 key);
```

 **keyHasPurpose**

Returns `TRUE` if a key is present and has the given purpose. If the key is not present it returns `FALSE`.

```
function keyHasPurpose(bytes32 _key, uint256 purpose) constant returns(bool exists);
```

 **getKeysByPurpose**

Returns an array of public key bytes32 held by this identity.

```
function getKeysByPurpose(uint256 _purpose) constant returns(bytes32[] keys);
```

 **addKey**

Adds a `_key` to the identity. The `_purpose` specifies the purpose of the key. Initially, we propose four purposes:

- `1`: MANAGEMENT keys, which can manage the identity
- `2`: EXECUTION keys, which perform executions in this identity's name (signing, logins, transactions, etc.)

MUST only be done by keys of purpose `1`, or the identity itself. If it's the identity itself, the approval process will determine its approval.

```
function addKey(bytes32 _key, uint256 _purpose, uint256 _keyType) returns (bool success)
```

 **removeKey**

Removes `_key` from the identity.

MUST only be done by keys of purpose `1`, or the identity itself. If it's the identity itself, the approval process will determine its approval.

```
function removeKey(bytes32 _key, uint256 _purpose) returns (bool success)
```

### Identity API

API Calls Specification
This document will outline the API specifications and how to use it:

POST /user/create_identity
The following endpoint creates a new digital identity based on ERC725 Standard and deploys it on the blockchain:
    let body = {
    "pwd": "myPassword",
    "name": "myName",
    "surname": "mySurname",
    "email": "myemail@myemail.com",
    "phone": "myPhone"
    }


```
// HTTP/1.1 200 OK
let response = {
"success": true,
"did": "0xbda2945e6a5757557366883ecba2cd20687afe72",
"address": "0x53a73dFFee40d3aCF833d113BcdCdA6A97b93F05",
"created_at": 2019-03-13T11:50:33+00:00,
"pwd": "myPassword",
"name": "myName",
"surname": "mySurname",
"email": "myemail@myemail.com",
"phone": "myPhone"
}
```

POST /user/add_claim
The following endpoint add a claim to the indicated DID
  let body = {
    "did": "0xbda2945e6a5757557366883ecba2cd20687afe72"
    }


```
// HTTP/1.1 200 OK
let response = {
"success": true,
"claim_id": 4294967295
}
```

POST /user/get_owner
The following endpoint receives a DID and returns the owner's address
    let body = {
    "did": "0xbda2945e6a5757557366883ecba2cd20687afe72"
    }
    // HTTP/1.1 200 OK
    let response = {
    "success": true,
    "address": "0x4D16Bf12709Df20c9cEd184675d95Bf3D54B48E5"
    }

GET /user/get_user_balance/{did}
The following endpoint returns the USDP balance for the indicated DID
    // HTTP/1.1 200 OK
    let response = {
    "success": true,
    "balance": 100.00
    }

GET /user/login?{mail}&{passw}
The following endpoint receives an email and a password and returns a Jason Web Token
    // HTTP/1.1 200 OK
    let response = {
    "success": true,
    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9"
    }



## Token Manager

### Token Description

**ERC20 Mintable and burnable stable Token.**

ERC20 is a technical standard used for smart contracts on the Ethereum blockchain for implementing tokens. ERC stands for Ethereum Request for Comment, and 20 is the number that was assigned to this request. The clear
majority of tokens issued on the Ethereum blockchain are ERC20 compliant.

ERC20 defines a common list of rules for Ethereum tokens to follow within the larger Ethereum ecosystem, allowing developers to accurately predict interaction between tokens. These rules include how the tokens are transferred between addresses and how data within each token is accessed ERC20 token has the following method-related functions.

The specific wording of the function is followed by a clarification of what it does.

1. totalSupply: Get the total token supply
2. balanceOf(address _owner) constant returns (uint256 balance): Get the
   account balance of another account with address _owner
3. transfer(address _to, uint256 _value) returns (bool success): Send _value amount of tokens to address _to]
4. transferFrom(address _from, address _to, uint256 _value) returns (bool
   success): Send _value amount of tokens from address _from to address _to]
5. approve(address _spender, uint256 _value) returns (bool success): Allow _spender to withdraw from your account, multiple times, up to the _value amount. If this function is called again it overwrites the current allowance with _value]
6. allowance(address _owner, address _spender) constant returns (uint256
   remaining): Returns the amount which _spender is still allowed to
   withdraw from _owner
   Events format:
7. Transfer(address indexed _from, address indexed _to, uint256 _value): Triggered when tokens are transferred.
8. Approval(address indexed _owner, address indexed _spender, uint256
   _value): Triggered whenever approve(address _spender, uint256 _value) is called.

#### Features.

- **Mintable:** New tokens ERC20 can be created by the Owner of the ERC20 under specific circumstances. For example, for a USDP new tokens will be created every time a client transfers money to one or more bank accounts owned (and audited).
- **Burnable**, tokens can be destroyed by the owner of the contract. In the case of USDP, tokens will be destroyed every time the client asks to get the money back from the bank account.
- **Pausable**, can be frozen if it's not compliant with the law.

When the token will be created the user has to select which features need for the token.

### Token Economy

An economy model is understood as the one, which applying a series of algorithms, creates standards for the currency to be useful in the network, to bring value to the user when using it, and to build rules to protect the currency from the market volatility or the price manipulation.

The token economy can be summarized in five parts:


*   *Connection power*: the value of connectivity is substantially enhanced as the size of blockchain burgeons on account of vast quantity of nodes joining in. Therefore will be a voting process for the nodes that will be activated on the blockchain.
*   *Exponential value*: when blockchain economics apply in a market, exponential value inevitably follows. Blockchain economy is a combination of economies and trust. An incentive for the users will be applied.
*   *Increasing returns*: an algorithm will be applied to increase the portfolio of each network user by holding the tokens.
*   *Allegiance*: consensus protocol based on the Aura algorithms that will be working on the network.
*   *Devolution*: economic transformation will be built on large-scale experimentation, and the process will most certainly not always be linear. A monthly/year return is calculated to verify the TTT growths.

A utility token provides value in the network, creating a series of rules for which the user wishes to be part of this blockchain implementation and the network growing.

The solution is to create a financial product that is protected from the market makers manipulation and be able to generate benefit to the user for being part of the network.

TTT token will be pre-mined having, therefore, a fixed supply from the beginning. Every token is distributed among the contributors, but for the generated model, much of the supply will be locked, to protect the user from price manipulation, and leaving in circulation a percentage of tokens.   

When investing in an asset, the first thing that is studied is the annual benefit - ROI that it can offer. In crypto it is difficult to predict as of how the market is manipulated, therefore, the developed model will stand out as a traditional financial product working in the blockchain.

To study that ROI, can be started from the Quantity Theory Of Money, known as the flow of money needed to support an economy, *M·V=P·Q*, we could study each parameter and try to predict the price that the token will have in a given year, for example. Let's develop this approach:

*   M= money supply in active circulation
*   V= velocity of money
*   P= price level of the currency
*   Q= output level of economic activity transacted

In short, we are comparing the supply with the demand,  the total amount of currency changing hands vs. total amount of goods being exchanged during a specific time.

As the token model includes a limited supply, the main goal is to increase the demand. The only parameter left to analyze is the Velocity as:

```
Velocity = Total transaction volume / Average Network value
```

Then the network value can be calculated as:

```
Average network value = Total transaction volume / velocity
```



The solution consists in decreasing the velocity of the previous equation. For TTT project,

we have called it "Staking and Distribution" functions.


![token_stored_in_wallet](/img/token_stored_in_wallet.JPG)


To have this working we have to focus on the Network effects that we want to get with TTT tokens. A product becomes more valuable to its existing users as more people use it.

The Network effect in MBAEX isn't the liquidity between USDP and TTT, but rather the network effect of the tokens being used for the users.

The more users hold it, the more valuable it becomes. It has been built a reward program for the user who decides to buy the tokens and store them in their wallets, to stake the tokens instead of buying for a speculative standpoint.


![token_supply](/img/token_supply.JPG)


In order to have the network working successfully, to run a node or send transactions, a use of the TTT token is build, using it as a necessary package for the correct executions within the blockchain network.

#### Token unlock program

The importance of the network growing is the number of tokens stored in the user wallets. With that perspective, a financial product can be build based on the number of tokens that the user hold.

The mainnet will start with 20% of the tokens in circulation. As the network grows the same each month will be distributed the active circulation increase between the network.

Increasing returns are applied for the users that hold the tokens in their respective wallet. Validator and Regular Nodes.

Method explanation:  

As the only 20% of the tokens will be in active circulation, each month the tokens will be distributed as per the next formula:


```
d = c/p %
```




*   d= distributed tokens
*   c= active circulation
*   p= period


### Token Node

A token node is a regular node that relies on validator nodes for many operations, in this particular case token nodes has also a smart contracts for the ERC20 that controls the token. This nodes only create and manage the tokens inside the network.

##### Steps for install a new token node

Account Client team will be the responsible to receive all the petitions to create new tokens. It will proceed to analyse the token/ asset that was requested. Once it will be reviewed and approved.  Contract

After approval the token enter in the whitelist

##### Step 1. Install a regular node.

Follow the instructions to install a regular node.

Create a new account (protected by password). You will get the new public Key which will be used to admin your token.

##### Step 2. Install and launch

```shell
bash:$ npm install
bash:$ export NODE_ENV={development, production}
bash:$ nodemon start
```

##### Step 3. Update config file

You need to setup the Token information inside config/default.json

Generic information

```json
"blockchain": {
    "rpc_endpoint": "http://localhost:8545",
    "admin": "0xnew_public_key",
    "pass" : "new_password"
}
```

Now set up your token configuration

```json
  "token" : {
    "symbol": "AAA",
    "name": "Token Name",
    "decimals": 2,
    "erc20" : ""
    "erc725" : ""
    "initialSupply" : 100000
  }
```

Also list the number of Tokens your Dapp will connect with

```json
  "tokens" : [{
    "symbol": "TTT",
    "name": "Mbaex Governance Token",
    "decimals": 2,
    "erc20" : ""
    "erc725" : ""
  },{
    "symbol": "MBT",
    "name": "Mbaex Fidelity Token",
    "decimals": 0,
    "erc20" : ""
    "erc725" : ""

  }]
```

##### Step 4. Deploy your token

```shell
bash:$ node deploy.js
```

### Tokens whitelist

There is a curated whitelist of tokens in the platform, this way we can avoid any kind of fraud.

The only way of transferring tokens is by using a ERC725 Smart Contract Identity, who will call the ERC20 contract asking to transfer the tokens.

To protect the system:

1. ERC20 will only allow transfers from ERC725 Smart Contracts (KYCed)
2. ERC725 will only transfer tokens from WhiteListed contracts.

### Front-end token dashboard

#### About

This is a dashboard to control the statics of the token node and manage the tokens in the web. Every node has one. The frontend will be responsible to show all the statistics related to the Blockchain node.

#### Features

- Users & Identity
  - Login with ERC725 Keys (Mobile App)
- Control all the data related to the node:
  - Blockchain Blocks and Transactions
  - Server Status (memory & CPU)
  - Token statistics
  - TTT Governance (validators)
    *![](img/Oc7STul.png)
- Create and manage new tokens
  - Token design
  - Staking & Minting
  - Transactions
  - User Balances & Transaction History
    ![](img/qVV2OOI.png)

## Mobile Wallet App

### Quick Guide

The token app will be a traditional wallet where users can store their platform tokens. This app requires users data as name and surname, then it creates a ERC725 and a pair of keys. Also, it stores all the tokens and displays the balance and the list of tokens owned by the user. And, it has the function transfer to buy and sell tokens like in an e-commerce never as a exchange.

### Wallet

As an important tool of communication, the wallet interface can provide end-users the possibility to understand and interact with the application.

The app is built as a wallet-like interface where users can view their current balance, transactions history and send and receive tokens.

In the future the wallet also will be used as a storage of claims for the identity.

![](img/oliX15R.png)

#### Transferring Tokens

The wallet will be the space in the app where the users store their keys and can transfer tokens between other users of the system.
